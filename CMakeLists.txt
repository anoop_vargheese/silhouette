cmake_minimum_required(VERSION 2.8)
add_definitions(-std=c++11)
project( silhouette )

find_package( OpenCV REQUIRED )
include_directories( include )

set(CMAKE_BUILD_TYPE Debug)

add_executable( silhouette 
    src/main.cpp 
    src/silhouette.cpp
    src/silhouettes.cpp
)

target_link_libraries( silhouette ${OpenCV_LIBS} )

if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "-O2")        ## Optimize
    set(CMAKE_EXE_LINKER_FLAGS "-s")  ## Strip binary
endif()
