
#include "silhouettes.h"
#include "neighdef.h"

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>


//Returns a vector of silhouettes, with each silhouettes being a vector of points
const std::vector< std::vector< cv::Point > >& Silhouettes::extractNBlobs(const cv::Mat_<float>& src, const int numFeatures)
{
	// Preprocess image to generate all member matrices
	preprocessImage(src);

	Silhouette detector;
	detector.processImage(imgMat);

	// Loop through heap-vector points, find silhouette for each one
	// Extract only the `numFeatures` number of best features
	while( !maxCurveHeapVec.empty() && silhouetteVec.size()<numFeatures)
	{
		// Extract next pre-seed
		WeightPoint wpt = maxCurveHeapVec.front();
		std::pop_heap(maxCurveHeapVec.begin(), maxCurveHeapVec.end());
		maxCurveHeapVec.pop_back();

		// Find local maxima near pre-seed to use as seed
		cv::Point seed = findNearestMaximum( gNormMat, cv::Point(wpt.x,wpt.y));
		// cv::Point seed = cv::Point(wpt.x, wpt.y);

		// Verify that seed does not already lie on previously extracted border
		if( maskMat(seed.y,seed.x) == false || (seed.y<10) || (seed.x<10) || (seed.y>maskMat.rows-10) || (seed.x>maskMat.cols-10) )
		{
			continue;
		}

		// Extract the feature border at the loop
		std::vector< cv::Point > featureVec;
		detector.detectBlob(seed, featureVec);

		// Add extracted feature to maskVec
		for(const cv::Point& pt : featureVec)
		{
			for(int i=-2; i<=2; ++i)
			{
				for(int j=-2; j<=2; ++j)
				{
					int x = pt.x + i;
					int y = pt.y + j;
					if( (x>0) && (y>0) && (x<maskMat.cols) && (y<maskMat.rows) )
					{
						maskMat(y,x) = false;
					}
				}
			}
		}

		// Add feature to database
		silhouetteVec.push_back(featureVec);

		//DEBUG Draw features as they are extracted
		// drawExtractedBlobs("output.jpg");
	}

	return silhouetteVec;
}

// Calculates gradients, gradient norms, divergence and curvature heuristic
void Silhouettes::preprocessImage(const cv::Mat_<float>& src)
{
	imgMat = src;
	maskMat = cv::Mat::ones(src.size(), cv::DataType<bool>::type); // Initialize maskMat as Mat of "true"
	silhouetteVec.clear();

	// Step 1 : Blur image to reduce noise
	cv::GaussianBlur(imgMat, imgMat, cv::Size(3,3), 0, 0);

	// Step 2 : Calculate gradients
	cv::Sobel(imgMat, gxMat, CV_32F, 1, 0, 3, 0.125);
	cv::Sobel(imgMat, gyMat, CV_32F, 0, 1, 3, 0.125);

	// Create copies of gradients to store the normalized values
	cv::Mat_<float> gxNormMat = gxMat.clone();
	cv::Mat_<float> gyNormMat = gyMat.clone();

	// Step 3 : Calculate norm of the gradient, with eps=0.1
	gNormMat = normalizeGradients(gxNormMat, gyNormMat, 0.1);

	// Step 4 : Calculate divergence of the normalized gradients
	cv::Mat_<float> gDivMat = calculateDivergence(gxNormMat, gyNormMat);

	// Step 5 : Calculate curvature heuristic of normalized gradient
	cv::multiply(gNormMat, gDivMat, gCurveHeurMat);

	// Step 6 : Calculate local extrema of curve heuristic
	maxCurveHeapVec = findLocalMaxima(gCurveHeurMat);
}

// Calculates a guarded norm of the two matrices, and normalize the gradients in the process
// norm = sqrt( x^2 + y^2 + eps )
// Uses sequential access of matrix elements
cv::Mat_<float> Silhouettes::normalizeGradients(cv::Mat_<float>& xMat, cv::Mat_<float>& yMat, const float eps) const
{
	cv::Mat_<float> normMat = cv::Mat(xMat.size(), CV_32F);

	float* pNorm = (float*)normMat.data; //Norm matrix iterator
	float* pX = (float*)xMat.data; //x matrix iterator
	float* pY = (float*)yMat.data; //y matrix iterator

	for (int i = 0; i < normMat.rows*normMat.cols; i++)
	{
		*pNorm = sqrt( (*pX)*(*pX) + (*pY)*(*pY) + eps);
		*pX = *pX / *pNorm;
		*pY = *pY / *pNorm;
		++pNorm;
		++pX;
		++pY;
	}

	return normMat; //Return r-value for move constructor
}

// Calculates the divergence of a vector field.
cv::Mat_<float> Silhouettes::calculateDivergence(const cv::Mat_<float>& xMat, const cv::Mat_<float>& yMat) const
{
	cv::Mat_<float> xxMat, yyMat;
	cv::Mat_<float> divMat = cv::Mat(xMat.size(), CV_32F);

	//Calculate derivatives of vector field
	cv::Sobel(xMat, xxMat, CV_32F, 1, 0, 3, 0.125);
	cv::Sobel(yMat, yyMat, CV_32F, 0, 1, 3, 0.125);

	//Add vector fields
	divMat = xxMat + yyMat;

	return divMat; //Return r-value for move constructor
}


// Find the local maxima of a matrix, and return a heap-vector
std::vector< WeightPoint > Silhouettes::findLocalMaxima(const cv::Mat_<float>& srcMat, const float thresh) const
{
	std::vector< WeightPoint > heapVec;
	cv::Mat_<float> tMat;

	// Blur matrix before finding extrema
	cv::GaussianBlur(srcMat, tMat, cv::Size(5,5), 0.7, 0.7);

	for (int y = 1; y < tMat.rows - 1; ++y) //loop y
	{
		for (int x = 1; x < tMat.cols - 1; ++x) //loop x
		{
			bool isMax = true;
			double center = tMat(y,x);
			if (abs(center) < thresh) continue;
			for (int i = 0; i < 8; i++) //loop border
			{
				int j = (3*i) % 8; // Change sample order to maximize efficiency
				if (center < tMat(y + y8arr[j], x + x8arr[j]))
				{
					isMax = false;
					break; //breaks border loop
				}
			}
			if(isMax)
			{
				heapVec.emplace_back(x,y,center);
			}
		} //end x loop
	} //end y loop

	// Sort all found points into heap
	make_heap(heapVec.begin(), heapVec.end());

	//DEBUG begin - View seed points
	// cv::Mat_<cv::Vec3f> outMat;
	// cv::cvtColor(imgMat, outMat, CV_GRAY2BGR);
	// outMat = outMat / (255 * 10);
	// cv::namedWindow("LocalMaxima", 1);
	// float maxHeur = heapVec.front().w;
	// for (WeightPoint& wpt : heapVec)
	// {
	// 	outMat(wpt.y, wpt.x)[2] = wpt.w / maxHeur;
	// }
	// cv::imshow("LocalMaxima",outMat);
	// cv::waitKey(0);
	// std::exit(0);
	//DEBUG end

	//DEBUG2 begin - View curve heuristic
	cv::imshow("Curve_Heuristic", srcMat * (1/heapVec.front().w/2 ) + 0.5 );
	cv::waitKey(0);
	//DEBUG2 end

	return heapVec; //Return r-value for move constructor
}


// Find the nearest local maxima to a point
cv::Point Silhouettes::findNearestMaximum(const cv::Mat_<float>& srcMat, cv::Point pt) const
{
	// Search for nearest maximum within a range of 10 pixels
	for(int i=1; i<=10; ++i)
	{
		int maxIdx = -1;
		float maxVal = srcMat(pt.y, pt.x);
		// Loop through border points
		for(int j=0; j<8; ++j)
		{
			if( maxVal < srcMat(pt.y + y8arr[j], pt.x + x8arr[j]) )
			{
				maxVal = srcMat(pt.y + y8arr[j], pt.x + x8arr[j]);
				maxIdx = j;
			}
		}
		if(maxIdx==-1) 
		{
			return pt;
		}
		// else update pt and repeat loop
		pt.x += x8arr[maxIdx];
		pt.y += y8arr[maxIdx];
	}
	std::cout<<"ERROR: Could not find maximum point at" << pt <<std::endl; //DEBUG
	return pt; // Return the last value of pt after 10 loops
}


// Draw each of the extracted blobs using a different color. Also outputs image to file
void Silhouettes::drawExtractedBlobs(const std::string filename) const
{
	cv::Vec3f color;
	const float rand_max = (float)RAND_MAX;

	std::srand(1);
	cv::Mat_<cv::Vec3f> outMat;
	cv::cvtColor(imgMat, outMat, CV_GRAY2BGR);
	outMat = outMat / (255 * 5);

	cv::namedWindow("Extracted_features", 1);

	for(const std::vector<cv::Point>& featureVec : silhouetteVec)
	{
		float t1 = std::rand()/rand_max * 3.14 / 2;
		float t2 = std::rand()/rand_max * 3.14 / 2;
		color = cv::Vec3f(std::cos(t1), std::sin(t1)*std::cos(t2), std::sin(t1)*std::sin(t2));
		for(const cv::Point& pt : featureVec)
		{
			outMat(pt.y, pt.x) = color;
		}
		//cv::imshow("Extracted_features", outMat);
		//cv::waitKey(0);
	}
	std::cout<<"Found " << silhouetteVec.size() << "features" << std::endl;
	cv::imshow("Extracted_features", outMat);
	cv::waitKey(0);

	if(filename!="")
	{
		cv::imwrite(filename,outMat*255);
	}
}
