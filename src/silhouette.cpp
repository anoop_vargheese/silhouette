#include <iostream>
#include <deque>
#include <array>
#include <vector>

#include <opencv2/opencv.hpp>

#include "neighdef.h"
#include "silhouette.h"

using namespace std;
using cv::Mat_;
using cv::Point;

using cv::Sobel;

//FIXME for the debug
using cv::imshow;
using cv::waitKey;

bool Silhouette::cached = false;
std::array<cv::Point, 24> Silhouette::stepArr;
std::array<double, 24> Silhouette::stepNormArr;

//#define SHOWIT

int cross(cv::Point u, cv::Point v)
{
    return u.x*v.y - u.y*v.x;
}

int dot(cv::Point u, cv::Point v)
{
    return u.x*v.x + u.y*v.y;
}

int direction(Point v)
{
    if (abs(v.x) > abs(v.y))
    {
        if (v.x > 0)
        {
            return int(floor(24 + v.y / double(v.x)) * 3) % 24;
        }
        else
        {
            return floor(12 + v.y / double(v.x) * 3);
        }
    }
    else
    {
        if (v.y > 0)
        {
            return floor(6 - v.x / double(v.y) * 3);
        }
        else
        {
            return floor(18 - v.x / double(v.y) * 3);
        }
    }
}

void IntersectionCounter::process(Point gap, int v2)
{
    double sin2 = cross(gap, Silhouette::stepArr[v2]);
    dirGap = direction(gap);
//    cout << sin1 << " " << sin2 << endl;
    switch (state)
    {
    case BASE:
        if (sin1*sin2 < 0)
        {
//            cout << v2 << " " << dirGap << " " << v1 << endl;
//            cout << diff<24>(v1, dirGap) << " " << diff<24>(v1, v2) << endl;
            int dv1g = diff<24>(v1, dirGap);
            int dv1v2 = diff<24>(v1, v2);
            if ((dv1g*dv1v2 > 0 and abs(dv1g) <= abs(dv1v2)) or abs(diff<24>(dirGap, v1)) < 3)
            {
                if (abs(diff<24>(v1, v2)) > 1) confirmed();
                else external(); 
            }
            else internal();
        }    
        break;
    case CHECK_EXTERNAL:
        if ((sin2 - sin1)*sin1 > 0) confirmed();
        else if (sin2*sin1 < 0) disproved();
        break;
    }
    if (sin2 != 0) sin1 = sin2;
    v1 = v2;
}

void IntersectionCounter::processBack(Point gap)
{
    double sin2 = cross(gap, Silhouette::stepArr[v1]);
    dirGap = direction(gap);
//    cout << gap << Silhouette::stepArr[v1]<< endl;
//    cout << sin1 << " " << sin2 << endl;
    switch (state)
    {
    case BASE:
        if (sin1*sin2 < 0)
        {
            if (abs(diff<24>(dirGap, v1)) < 6) external(); 
            else internal();
        }    
        break;
    case CHECK_EXTERNAL:
        if ((sin2 - sin1)*sin1 > 0) confirmed();
        else if (sin2*sin1 < 0) disproved();
        break;
    }
    if (sin2 != 0) sin1 = sin2;
}

/*
bool IntersectionCounter::allowed(double delta)
{
    cout <<  delta << " " <<   state << " " <<  mu << " " <<  turnCounter << endl;
    if (turnCounter < 0) return false;
    if (mu == 1 and turnCounter < 1)
    {
        if (delta < 0 and state == CHECK_EXTERNAL) return false;
    }
    return true;
}*/

bool IntersectionCounter::allowed(double delta, int v2)
{
//    cout <<  delta << " " <<   state << " " <<  mu << " " <<  turnCounter  << endl;
//    cout << dirGap << " " << v1 <<  " " << v2<< endl;
    if (turnCounter < 0) return false;
    if (mu == 1 and turnCounter < 1)
    {
        if (delta < 0)
        {
            if (state == CHECK_EXTERNAL) return false;
            int dv1g = diff<24>(v1, dirGap);
            int dv1v2 = diff<24>(v1, v2);
            if ((dv1g*dv1v2 > 0 and abs(dv1g) <= abs(dv1v2)) or abs(diff<24>(dirGap, v1)) < 3)
            {
                if (abs(diff<24>(v1, v2)) > 1) return false;
            }  
        }
    }
    return true;
}

void Silhouette::processImage(const cv::Mat_<float> & src)
{
    Sobel(src, gxMat, CV_32F, 1, 0, 3, 0.125);
    Sobel(src, gyMat, CV_32F, 0, 1, 3, 0.125);
    xMax = src.cols - 5;
    yMax = src.rows - 5;
    xMin = 5;
    yMin = 5;
    cols = src.cols / 2 + 1;
    rows = src.rows / 2 + 1;
    visitedVec.resize(cols*rows, false);
}

bool Silhouette::getVisited(cv::Point pt)
{
    return visitedVec[(pt.y/2) * cols + pt.x/2];
}
    
void Silhouette::setVisited(cv::Point pt)
{
    int x = pt.x/2;
    int y = pt.y/2;
    visitedVec[y * cols + x] = true;
    visitedVec[(y + 1) * cols + x] = true;
    visitedVec[y * cols + x + 1] = true;
    visitedVec[(y + 1) * cols + x + 1] = true;
}

void Silhouette::resetVisited(cv::Point pt)
{
    int x = pt.x/2;
    int y = pt.y/2;
    visitedVec[y * cols + x] = false;
    visitedVec[(y + 1) * cols + x] = false;
    visitedVec[y * cols + x + 1] = false;
    visitedVec[(y + 1) * cols + x + 1] = false;
}

Silhouette::Silhouette() : delay(5)
{
    if (not cached)
    {
        cached = true;
        for (int i = 0; i < 24; i++)
        {
            stepArr[i].x = x24arr[i];
            stepArr[i].y = y24arr[i];
            stepNormArr[i] = sqrt(dot(stepArr[i], stepArr[i]));
        }
    }
}

void Silhouette::detectBlob(Point seed, vector<Point> & outVec)
{
    state = INIT;
    p0 = seed;
    #ifdef SHOWIT
    Mat_<float> res(gxMat.size());
    res.setTo(0);
    #endif
    while (state != TERMINATED)
    {
        switch (state)
        {
        case INIT:
            state = initSearch();
            break;
        case INTEGRATE:
            state = integrate();
            #ifdef SHOWIT
            for (auto & p : curveDeq)
            {
                res(p) = 0;
                for (int i = 0; i < 8; i++)
                {
                    res(p.y + y8arr[i], p.x + x8arr[i]) = 250;
                }
            }
            imshow("res", res/255);
            waitKey();
            #endif
            break;
        case FINALIZE:
            state = finalize(outVec);
            break;
        }
    }
    #ifdef SHOWIT
    cout << "energy " << energy << endl;
    cout << "flow " << flow << endl;
    cout << "length " << length + gapLength << endl;
    #endif
}

void Silhouette::findMinMaxDir(Sense sense)
{
//    cout << "MIN MAX DETECTION " << sense << endl;
    Point pt = sense ? curveDeq.back() : curveDeq.front();
    const double gx = gxMat(pt);
    const double gy = gyMat(pt);
//    cout << "GRADIENT " << gx << " " << gy << endl;
    for (int i = 0; i < 24; i++)
    {
        Point dp = stepArr[i];
        double gx2 = 0.5*(gx + gxMat(pt + dp));
        double gy2 = 0.5*(gy + gyMat(pt + dp));
//        cout << i << " "  << gxMat(pt + dp) << " " << gyMat(pt + dp) << endl;
        double q = (gx2*dp.y - gy2*dp.x) / stepNormArr[i];
        minMaxArr[sense].valArr[i] = q;
    
    }
    minMaxArr[sense].medianFilter();
    minMaxArr[sense].findMinMax(energy/10);
}

Silhouette::IntersectonTrackResult Silhouette::findIntersection(Sense sense)
{
    Point pt;
    int count = 0;
    auto ptIt1 = curveDeq.begin() + delay;
    auto ptIt2 = curveDeq.rbegin() + delay;
    switch (sense)
    {
    case FORWARD:
        pt = curveDeq.front();
        while (norm(pt - *ptIt1) > 5) 
        {
            ptIt1++;
            count++;
        }
        break;
    case BACKWARD:
        pt = curveDeq.back();
        while (norm(pt - *ptIt2) > 5) 
        {
            ptIt2++;
            count++;
        }
        break;    
    }
//    cout << count << " " << lengthArr[sense] << endl;
    if (count > lengthArr[sense] or count < lengthArr[sense] - branchVecArr[sense].back() ) 
    {
//        cout << "FINAL" << endl;
        return FINAL; //FIXME
    }
    else
    {
//        cout << "INTERMEDIAT" << endl;
        return INTERMEDIAT;
    }
}

void Silhouette::popPoint(Sense sense)
{
    int     dir = (prevDirArr[sense] + 12) % 24;
    Point dp2;
    if (lengthArr[sense] > delay)
    {
        Point delayedPt = sense ? *(curveDeq.rbegin() + delay) : *(curveDeq.begin() + delay);
        resetVisited(delayedPt);
    }
    lengthArr[sense]--;
    switch (sense)
    {
    case FORWARD:
        curveDeq.pop_front();
        {
            auto ptIt1 = curveDeq.begin();
            dp2 = ptIt1[0] - ptIt1[1];
        }
        flow += minMaxArr[sense].valArr[dir] * stepNormArr[dir];
        break;
    case BACKWARD:
        curveDeq.pop_back();
        {
            auto ptIt2 = curveDeq.rbegin();
            dp2 = ptIt2[0] - ptIt2[1];
        }
        flow -= minMaxArr[sense].valArr[dir] * stepNormArr[dir];
        break;    
    }
    prevDirArr[sense] = prev2DirArr[sense];
    prev2DirArr[sense] = direction(dp2);
    length -= stepNormArr[dir];
    gap = curveDeq.front() - curveDeq.back();
    gapLengthPrev = gapLength;
    gapLength = norm(gap);
    energy = flow / (length + gapLength);
    maxFlow = flow / length;
    
}

Silhouette::InsertionResult Silhouette::insertPoint(const int dir, Sense sense)
{
    Point newPt;
    Point prevPt;
    
    bool branching = (sense ? minMaxArr[sense].minIdxVec.size() : minMaxArr[sense].maxIdxVec.size()) > 1;
    if (branching) 
    {
        branchVecArr[sense].push_back(lengthArr[sense]);
    }
    lengthArr[sense]++;
        
    switch (sense)
    {
    case FORWARD:
        prevPt = curveDeq.front();
        newPt = prevPt + stepArr[dir];
        curveDeq.push_front(newPt);
        flow += minMaxArr[sense].valArr[dir] * stepNormArr[dir];
        break;
    case BACKWARD:
        prevPt = curveDeq .back();
        newPt = prevPt + stepArr[dir];
        curveDeq.push_back(newPt);
        flow -= minMaxArr[sense].valArr[dir] * stepNormArr[dir];
        break;    
    }
    prev2DirArr[sense] = prevDirArr[sense];
    prevDirArr[sense] = dir;
    length += stepNormArr[dir];
    gap = curveDeq.front() - curveDeq.back();
    gapLengthPrev = gapLength;
    gapLength = norm(gap);
    energy = flow / (length + gapLength);
    maxFlow = flow / length;
    
    if (getVisited(newPt))
    {
        if (lengthArr[sense] < delay) return FAILURE;
        auto res = findIntersection(sense);
        switch (res)
        {
        case FINAL:
            return INTERSECTION;
        case INTERMEDIAT:
            while (lengthArr[sense] > branchVecArr[sense].back())
            {
                popPoint(sense);
            }
            return INTERSECTION; // FIXME       
        }
    }
    
    if (lengthArr[sense] > delay)
    {
        Point delayedPt = sense ? *(curveDeq.rbegin() + delay) : *(curveDeq.begin() + delay);
        setVisited(delayedPt);
    }
    
    const int eta = sense ? -1 : 1;
//    cout << "FORWARD" << endl;
    counterArr[sense].process(eta*gap, dir);
//    cout << "BACKWARD" << endl;
    counterArr[1 - sense].processBack(-eta*gap);
//    
//        cout << "TURNS " << counterArr[FORWARD].turnCounter << " " 
//                << counterArr[BACKWARD].turnCounter << endl;
//        cout << "MUs " << counterArr[FORWARD].mu << " " 
//                << counterArr[BACKWARD].mu << endl;
//        cout << "STATES " << counterArr[FORWARD].state << " " 
//                << counterArr[BACKWARD].state << endl << endl;
                
    if (newPt.x < xMin or newPt.y < yMin or newPt.x > xMax or newPt.y > yMax)
    {
        return IMAGE_BORDER;
    }
    return SUCCESS;
}

Silhouette::ProcessState Silhouette::initSearch()
{
    //init the curve
    length = 0;
    flow = 0;
    energy = 0;
    curveDeq.clear();
    curveDeq.push_front(p0);
    lengthArr.fill(0);
    branchVecArr[FORWARD].clear();
    branchVecArr[BACKWARD].clear();
    
//    intersectionCounterArr.fill(IntersectionCounter());
    // Find first two steps
    findMinMaxDir(FORWARD);
    prevDirArr[FORWARD] = minMaxArr[FORWARD].globalMaxIdx();
    if (prevDirArr[FORWARD] == -1) return TERMINATED;
    minMaxArr[BACKWARD] = minMaxArr[FORWARD];
    prevDirArr[BACKWARD] = minMaxArr[BACKWARD].globalMinIdx();
    if (prevDirArr[BACKWARD] == -1) return TERMINATED;
    
    // Check convexity
    prev2DirArr[FORWARD] = (prevDirArr[BACKWARD] + 12) % 24;
    prev2DirArr[BACKWARD] = (prevDirArr[FORWARD] + 12) % 24;
    if (diff<24>(prevDirArr[FORWARD], prev2DirArr[FORWARD]) < -1) return TERMINATED;
    counterArr.fill(IntersectionCounter());
    counterArr[FORWARD].v1 = prevDirArr[FORWARD];
    counterArr[FORWARD].sin1 = 0.1;
    counterArr[BACKWARD].v1 = prevDirArr[BACKWARD];
    counterArr[BACKWARD].sin1 = -0.1;
    
    insertPoint(prevDirArr[FORWARD], FORWARD);
    insertPoint(prevDirArr[BACKWARD], BACKWARD);
    
    // Prepare for the further search
    findMinMaxDir(FORWARD);
    findMinMaxDir(BACKWARD);
    return INTEGRATE;
}

pair<int, double> Silhouette::detectBestDirection(Sense sense)
{
    const auto & idxVec = sense ? minMaxArr[sense].minIdxVec : minMaxArr[sense].maxIdxVec;
    const int eta = sense ? -1 : 1;
    int res = -1;
    int prevDir = prevDirArr[sense];
    double bestCost = -1e10;
    for (auto idx : idxVec)
    {
//        cout << "dir " << idx << endl;
        int turn = diff<24>(idx, prevDir);
//        if (abs(turn) > 9) continue;
        double dLgap = eta*dot(gap, stepArr[idx]) / gapLength;
        // Change in the gap direction
        double dtheta = cross(gap, stepArr[idx])/gapLength/gapLength;
        // Change in the curve direction
        double dalpha = eta*turn * M_PI / 12;
//        eta*cross(stepArr[prevDir], stepArr[idx]);
//        dalpha /= stepNormArr[idx] * stepNormArr[prevDir];
        // Their difference is the change in the angle between the curve and the gap
        if (not counterArr[sense].allowed(dalpha - dtheta, idx)) continue;
//        if (not counterArr[1 - sense].allowed(dtheta)) continue;
//        cout << "ALLOWED" << endl;
        double dflow = eta*minMaxArr[sense].valArr[idx];
        dflow = min(dflow, energy)*stepNormArr[idx];
//        cout << dflow << endl;
        double dlength = stepNormArr[idx] + dLgap; 
        double cost = dflow / energy - dlength;
        if (bestCost < cost)
        {
            bestCost = cost;
            res = idx;
        }
    }
    return make_pair(res, bestCost);
}

pair<int, double> Silhouette::popCostChange(Sense sense)
{
    const int idx = (prevDirArr[sense] + 12) % 24; 
    const int eta = sense ? -1 : 1;
    Point step = stepArr[idx];
    double dlength = -stepNormArr[idx] + eta*dot(gap, step) / gapLength;
    Point pt = sense ? curveDeq.back() : curveDeq.front();
    const double gx = gxMat(pt);
    const double gy = gyMat(pt);
    double gx2 = 0.5*(gx + gxMat(pt + step));
    double gy2 = 0.5*(gy + gyMat(pt + step));
    double dflow = eta*(gx2*step.y - gy2*step.x);
    return  make_pair(idx, dflow / energy - dlength);
}

//TODO select the best edge to go
Silhouette::ProcessState Silhouette::integrate()
{
//    cout << "FORWARD" << endl;
    auto bestForward = detectBestDirection(FORWARD);
//    cout << "BACKWARD" << endl; 
    auto bestBackward = detectBestDirection(BACKWARD);
    auto popForward = popCostChange(FORWARD);
    auto popBackward = popCostChange(FORWARD);
    InsertionResult result = NONE;
    // Compare two directions
    
//    //FIXME just a temoporary structure to see the result
//    if (popBackward.second < 0 and popForward.secon < 0)
//    {
        if ( bestForward.first != -1 and (not bestBackward.first == -1 or 
            bestForward.second > bestBackward.second) )
        {
            result = insertPoint(bestForward.first, FORWARD);
            if (result == SUCCESS) findMinMaxDir(FORWARD);
        }
        else if (bestBackward.first != -1)
        {
            result = insertPoint(bestBackward.first, BACKWARD);
            if (result == SUCCESS) findMinMaxDir(BACKWARD);
        }
        if (result != SUCCESS or gapLength < 5) 
        {
            if (result == CONCAVITY_DETECTED) return TERMINATED;
            else return FINALIZE;
        }
        return INTEGRATE;
//    }
//    else
//    {
//       auto winner1 = bestForward.second > bestBackward.second ? bestForward : bestBackward
//    }
}

Silhouette::ProcessState Silhouette::finalize(vector<Point> & outVec)
{
    outVec.reserve(curveDeq.size());
    for (auto & pt : curveDeq)
    {
        outVec.push_back(pt);
        resetVisited(pt);
    }
    return TERMINATED;
}


