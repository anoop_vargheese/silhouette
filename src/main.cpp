#include <ctime>

#include <opencv2/opencv.hpp>

#include "silhouettes.h"
#include "silhouette.h"
#include "neighdef.h"

using namespace cv;
using namespace std;

#define MIRE
#define SHOWIT

int runPreseededMain (int argc, char const* argv[])
{

#if defined(CIRCLE)
    const int radius = 450;
    const int imageSize = 1000;
    Mat_<float> img(imageSize, imageSize);
    img.setTo(120);
    for (int i = 0; i < img.rows; i++)
    {
        for (int j = 0; j < img.cols; j++)
        {
            int r = (i-imageSize/2)*(i-imageSize/2) + (j-imageSize/2)*(j-imageSize/2);
            if (r > radius*radius - 1 /*or r < 20*20 - 1*/) continue;
            img(i, j) = 0;
        }
    }
    
    vector<Point> seedVec {Point(imageSize/2, imageSize/2 + radius)};
    
#elif defined(LAMP)
    Mat_<float> img = imread("/home/bogdan/projects/stack/view0.png", 0);
    vector<Point> seedVec {
                            Point(1297, 655), Point(699, 980), Point(980, 976),
                            Point(951, 974),
                            Point(1347, 644), Point(1349, 630)};
#elif defined(LENA)
    Mat_<float> img = imread("/home/bogdan/projects/stack/lena.png", 0);
    vector<Point> seedVec {Point(138, 254), Point(266, 267), Point(288, 264), Point(296, 321), 
                Point(270, 349), Point(159, 385), Point(321, 392), Point(297, 365)};

#elif defined(MIRE)
    Mat_<float> img = imread("/home/bogdan/projects/mire.png", 0);
    vector<Point> seedVec { 
                            Point(930, 263), Point(985, 416),  Point(971, 499), 
                            Point(945, 394), Point(937, 369), 
                            Point(1006, 419), 
                            Point(564, 368), Point(578, 409), 
                            Point(628, 801), Point(605, 804), Point(641, 816), Point(1005, 344),
                            Point(933, 768)};
#elif defined(DROP)
    Mat_<float> img = imread("/home/bogdan/projects/test1.png", 0);
    vector<Point> seedVec {Point(141, 246)};

#elif defined(CONCAVE)
    Mat_<float> img = imread("/home/bogdan/projects/test3.png", 0);
    vector<Point> seedVec {Point(209, 111), Point(36, 292 ), Point(136, 240),
                            Point(202, 50), Point(84, 97)};
    
#endif
    GaussianBlur(img, img, Size(3, 3), 0, 0);
    #ifdef SHOWIT
    Mat_<Vec3f> outMat;
    cvtColor(img, outMat, CV_GRAY2BGR);
    #endif
    Silhouette detector;
    detector.processImage(img);
    auto t1 = clock();  
    vector<Point> resVec;
    for (auto & pt : seedVec)
    {
        resVec.clear();
        detector.detectBlob(pt, resVec);
        cout << resVec.size() << endl;
        #ifdef SHOWIT
        for (auto & p : resVec)
        {
            outMat(p) = 0;
            for (int i = 0; i < 8; i++)
            {
                outMat(p.y + y8arr[i], p.x + x8arr[i]) = Vec3f(0, 250, 0);
            }
            
        }
        imshow("res", outMat/255);
        waitKey();
        #endif
    }              
    auto t2 = clock();
    cout << double(t2 - t1) / CLOCKS_PER_SEC << endl;

    return 0;
}


int runUnseededMain(int argc, char const* argv[])
{
    Mat_<float> img;
    std::string outfile = "output";
    int numFeatures = 400;

    switch(argc)
    {
        case 4:
            numFeatures = std::atoi(argv[3]);
            cout << "Target number of features to extract : " << numFeatures << endl;
        case 3:
            outfile = argv[2];
            cout << "Output file selected : " << (outfile+"#.jpg") << endl;
        case 2:
            //Force image to open in grayscale
            img = imread(argv[1], 0);
            break;
        case 1:
        default:
            cout<< "Usage: " << argv[0] << " src_image [output_image] [num_features] " << endl;
            return 1;
            break;
    }


    if(!img.data)
    {
        cout << "ERROR : Image not found" << endl;
        return 1;
    }

    //Resize for debug purposes
    cv::resize(img, img, cv::Size(800,600));

    GaussianBlur(img, img, Size(3,3), 0, 0);

    Silhouettes extractor;

    auto t1 = clock();
    extractor.extractNBlobs(img, numFeatures);
    extractor.drawExtractedBlobs(outfile+"1.jpg");
    extractor.extractNBlobs(255-img, numFeatures);
    extractor.drawExtractedBlobs(outfile+"2.jpg");
    auto t2 = clock();

    cout << "Total time: " << (double)(t2-t1) / CLOCKS_PER_SEC << " seconds" << endl;
    // extractor.drawExtractedBlobs(outfile);

    return 0;
}


int main(int argc, char const* argv[])
{
    return runUnseededMain(argc, argv);
}
