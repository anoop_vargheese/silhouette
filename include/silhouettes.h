#pragma once

#include <vector>
#include <deque>
#include <algorithm>

#include <opencv2/opencv.hpp>

#include "silhouette.h"

struct WeightPoint
{
	WeightPoint() {};
	WeightPoint(int x, int y, double w) : x(x), y(y), w(w) {};
	bool inline operator<(const WeightPoint& wpt2) { return (this->w < wpt2.w); };

	int x, y;
	double w; //The curvature at point (x,y)
};

class Silhouettes
{
public:
	Silhouettes() {};
	virtual ~Silhouettes() {};

	const std::vector< std::vector <cv::Point> >& extractNBlobs(const cv::Mat_<float>& src, const int numFeatures = 30);

	void drawExtractedBlobs(const std::string filename = "") const; // Method for debugging code

private:
	void preprocessImage(const cv::Mat_<float>& src);

	cv::Mat_<float> normalizeGradients(cv::Mat_<float>& xMat, cv::Mat_<float>& yMat, const float eps = 0.1) const;

	cv::Mat_<float> calculateDivergence(const cv::Mat_<float>& xMat, const cv::Mat_<float>& yMat) const;

	std::vector<WeightPoint> findLocalMaxima(const cv::Mat_<float>& srcMat, const float thresh = 0.1) const;

	cv::Point findNearestMaximum(const cv::Mat_<float>& srcMat, cv::Point pt) const;

private:
	std::vector< std::vector<cv::Point> > silhouetteVec;

	cv::Mat_<float> imgMat; //The original image
	cv::Mat_<float> gxMat, gyMat; //Gradient matrices
	cv::Mat_<float> gNormMat; //Modified norm of the gradients
	cv::Mat_<float> gCurveHeurMat; //Curvature heuristic to find seeds
	cv::Mat_<bool> maskMat; //Stores whether to search point, and if point has already been discovered

	std::vector<WeightPoint> maxCurveHeapVec; //Heap-vector of all the maximum curvature points
};

