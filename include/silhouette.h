#pragma once

#include <deque>
#include <array>
#include <vector>

#include <opencv2/opencv.hpp>

class Silhouettes; //Defined here so that it can be friended by the Silhouette class

int cross(cv::Point u, cv::Point v);

int dot(cv::Point u, cv::Point v);

template <int N>
int diff(int a, int b)
{
    if (abs(a - b) <= N/2) return a - b;
    else
    {
        if (a > N/2) a -= N;
        else if (b > N/2) b -= N;
        return a - b;
    }
}

template <int N>
int checkOrder(int a, int b, int c)
{
    b = (b + N - a) % N;
    c = (b + N - a) % N;
    return b < c;
}

template <int N>
struct MinMax
{
    void findMinMax(double thresh)
    {
        minIdxVec.clear();
        maxIdxVec.clear();
        for (int i = 0; i < N; i++)
        {
//            std::cout << valArr[i] << " ";
            int j = (i + 1) % N;
            
            int k = (i + 2) % N;
            if (valArr[j] > thresh and valArr[j] >= valArr[i] and valArr[j] >= valArr[k]) 
            {
                maxIdxVec.push_back(j);
            }
            if (valArr[j] < -thresh and valArr[j] <= valArr[i] and valArr[j] <= valArr[k]) 
            {
                minIdxVec.push_back(j);
            }
        }
//        std::cout << std::endl;
    }
    
    int globalMinIdx()
    {
        int res = -1;
        double val = 0;
        for (auto & i : minIdxVec)
        {
            if (valArr[i] < val)
            {
                val = valArr[i];
                res = i;
            }
        }
        return res;
    }
    
    int globalMaxIdx()
    {
        int res = -1;
        double val = 0;
        for (auto & i : maxIdxVec)
        {
            if (valArr[i] > val)
            {
                val = valArr[i];
                res = i;
            }
        }
        return res;
    }
    
    void medianFilter()
    {
        std::array<double, N> resArr;
        for (int i = 0; i < N; i++)
        {
            int a = valArr[i];
            int j = (i + 1) % N;
            int b = valArr[j];
            int c = valArr[(i + 2) % N];
            resArr[j] = std::max(std::min(a,b), std::min(std::max(a,b),c));
        }
        for (int i = 0; i < N; i++)
        {
            valArr[i] = 0.5*(resArr[i] + valArr[i]);
        }
    }   
    
    std::array<double, N> valArr;
    std::vector<int> minIdxVec;
    std::vector<int> maxIdxVec;
};

int direction(cv::Point v);

struct IntersectionCounter
{
enum State {BASE, CHECK_EXTERNAL};

    IntersectionCounter() : turnCounter(0), mu(1), state(BASE), v1(0) {}
    
    void process(cv::Point gap, int v2);
    void processBack(cv::Point gap);
    
    void internal() 
    {
        mu = -mu;
    }
    
    void external()
    {
        state = CHECK_EXTERNAL;
    }
    
    void confirmed()
    {
        mu = -mu;
        turnCounter += mu;
        state = BASE;
    }
    
    void disproved()
    {
        state = BASE;
    }

    bool allowed(double delta, int v2);
//    bool allowed(double delta);
    
    State state;
    int dirGap;
    int v1;
    double sin1;
    int turnCounter;
    int mu;
};

class Silhouette
{   

    friend class Silhouettes; //FIXME : Is this necessary?

enum Sense : int {FORWARD = 0, BACKWARD = 1};
enum ProcessState {INIT, DETECT_CONTOUR, INTEGRATE, FINALIZE, TERMINATED};
enum ConvexFlag {CONVEX, CONCAVE, UNKNOWN};
enum VisitedFlag : uint8_t  {EMPTY = 0, WARM = 1, COLD = 3};
enum InsertionResult {NONE, FAILURE, SUCCESS, CONCAVITY_DETECTED, IMAGE_BORDER, INTERSECTION};
enum IntersectonTrackResult {FINAL, INTERMEDIAT};
public:
    Silhouette();
    virtual ~Silhouette() {}

    void processImage(const cv::Mat_<float> & src);
    
    void detectBlob(cv::Point seed, std::vector<cv::Point> & outVec);
    
    void findMinMaxDir(Sense sense);
    
    void initialConvexityCheck(const int dir, Sense sense);
    
    InsertionResult insertPoint(const int dir, Sense sense);
    void popPoint(Sense sense);
    
    std::pair<int, double> detectBestDirection(Sense sense);
    
    std::pair<int, double> popCostChange(Sense sense);
    
    IntersectonTrackResult findIntersection(Sense sense);
    
    bool getVisited(cv::Point pt);
    
    void setVisited(cv::Point pt);
    
    void resetVisited(cv::Point pt);
    
    //Finite automaton functions
    ProcessState initSearch();
//    ProcessState detectContour();
    ProcessState integrate();
    ProcessState finalize(std::vector<cv::Point> & outVec);
    
    static std::array<cv::Point, 24> stepArr;
    static std::array<double, 24> stepNormArr; 
    static bool cached;
    
private:
    cv::Mat_<float> gxMat, gyMat;
//FIXME    cv::Mat_<bool> visitedMat;
    int xMax, yMax;
    int xMin, yMin;
    
    ProcessState state;
    cv::Point p0, gap;
    std::deque<cv::Point> curveDeq;
    
    std::array<int, 2> lengthArr;
    std::array<std::vector<int>, 2> branchVecArr;
    double gapLength, length, flow, energy, gapLengthPrev;
    double maxFlow;
    
    // self-intersection detection
    std::vector<bool> visitedVec;
    int cols, rows;
    int delay;
    
    
    std::array<int, 2> prevDirArr;
    std::array<int, 2> prev2DirArr;
    std::array<MinMax<24>, 2> minMaxArr;
    std::array<IntersectionCounter, 2> counterArr;
};

